﻿using System;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace Jls.Web.UI.TestFramework
{
    public static class WebDriverExtensions
    {
        public static void OpenInNewTab(this IWebDriver driver, IWebElement element)
        {
            new Actions(driver)
                .MoveToElement(element)
                .KeyDown(Keys.LeftControl)
                .Click()
                .Release()
                .KeyUp(Keys.LeftControl)
                .Perform();

            driver.SwitchToLastTab();
        }

        public static void CloseCurrentTab(this IWebDriver driver)
        {
            if (driver.WindowHandles.Count < 2)
            {
                return;
            }

            driver.Close();
            SwitchToLastTab(driver);
        }

        public static void SwitchToLastTab(this IWebDriver driver)
        {
            var count = driver.WindowHandles.Count;
            driver.SwitchTo().Window(driver.WindowHandles[count - 1]);
        }

        public static void SwitchToMainTab(this IWebDriver driver)
        {
            driver.SwitchTo().Window(driver.WindowHandles[0]);
        }

        public static void Pause(this IWebDriver driver, int milliseconds = 1000)
        {
            Thread.Sleep(milliseconds);
        }

        public static void WaitUntilVisible(this IWebDriver driver, By selector, int timeoutSeconds, string errorMsg = null)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutSeconds));
                wait.Until(ExpectedConditions.ElementIsVisible(selector));
            }
            catch (WebDriverTimeoutException)
            {
                if (string.IsNullOrWhiteSpace(errorMsg))
                {
                    Assert.Inconclusive($"Timed out waiting for an element ({selector}) to become visible.");
                }

                Assert.Inconclusive(errorMsg);
            }
        }

        public static void WaitUntilVisible(this IWebDriver driver, IWebElement element, int timeout, string errorMsg = null)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                wait.Until(d => element.Displayed);
            }
            catch (WebDriverTimeoutException)
            {
                if (string.IsNullOrWhiteSpace(errorMsg))
                {
                    Assert.Inconclusive($"Timed out waiting for an element ({element}) to become visible.");
                }

                Assert.Inconclusive(errorMsg);
            }
        }

        public static void WaitUntilNotVisible(this IWebDriver driver, By selector, int timeout, string errorMsg = null)
        {
            if (!driver.ElementExists(selector))
            {
                return;
            }

            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(selector));
            }
            catch (WebDriverTimeoutException)
            {
                if (string.IsNullOrWhiteSpace(errorMsg))
                {
                    Assert.Inconclusive($"Timed out waiting for an element ({selector}) to disappear from the screen.");
                }

                Assert.Inconclusive(errorMsg);
            }
        }

        public static void WaitUntilNotExist(this IWebDriver driver, By selector, int timeout, string errorMsg = null)
        {
            if (!driver.ElementExists(selector))
            {
                return;
            }

            try
            {
                var element = driver.FindElements(selector).FirstOrDefault();
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                wait.Until(ExpectedConditions.StalenessOf(element));
            }
            catch (WebDriverTimeoutException)
            {
                if (string.IsNullOrWhiteSpace(errorMsg))
                {
                    Assert.Inconclusive($"Timed out waiting for an element ({selector}) to disappear from the screen.");
                }

                Assert.Inconclusive(errorMsg);
            }
        }

        public static bool ElementExists(this IWebDriver driver, By selector)
        {
            var elements = driver.FindElements(selector);
            return elements.Any();
        }

        public static void WaitUntilClickable(this IWebDriver driver, IWebElement element, int timeout, string errorMsg = null)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                wait.Until(a => element.IsClickable());
            }
            catch (WebDriverTimeoutException)
            {
                if (string.IsNullOrWhiteSpace(errorMsg))
                {
                    throw;
                }

                throw new WebDriverTimeoutException(errorMsg);
            }
        }

        public static void WaitUntilDomReady(this IWebDriver driver)
        {
            // Make sure DOM is ready
            IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(d => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public static void ScrollToElement(this IWebDriver driver, IWebElement element)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
        }

        public static void RefreshPage(this IWebDriver driver)
        {
            driver.Navigate().Refresh();
        }
    }
}
