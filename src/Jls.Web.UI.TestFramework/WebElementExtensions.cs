﻿using System;
using System.Threading;
using OpenQA.Selenium;

namespace Jls.Web.UI.TestFramework
{
    public static class WebElementExtensions
    {
        public static void ClickElement(this IWebElement element)
        {
            Thread.Sleep(250);
            var driver = JlsWebDriver.GetInstance();
            var timeout = 30;

            try
            {
                driver.WaitUntilClickable(element, timeout);
                element.Click();
            }
            catch (Exception e)
            {
                if (e.Message.ToUpperInvariant().Contains("IS NOT CLICKABLE AT POINT"))
                {
                    driver.ScrollToElement(element);
                    driver.Pause(300);
                    element.Click();
                }
            }

            Thread.Sleep(500);

            driver.WaitUntilNotVisible(By.Id("crm-overlay"), timeout, "Timed out waiting for the 'loading' spinner to disappear.");
        }

        public static bool IsClickable(this IWebElement element)
        {
            var value = element.GetCssValue("pointer-events");
            if (value == null || value.Contains("none"))
            {
                return false;
            }
            return true;
        }
    }
}
