﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Jls.Web.UI.TestFramework
{
    public class JlsWebDriver
    {
        public static int TimeoutInSeconds = 60;
        private static ChromeDriver _driver;

        public static ChromeDriver GetInstance()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("--start-maximized");
            chromeOptions.AddArgument("--incognito");

            if (_driver == null)
            {
                _driver = new ChromeDriver(chromeOptions);
                //_driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(TimeoutInSeconds);
                //_driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            }

            return _driver;
        }

        public static void Dispose()
        {
            _driver?.Quit();
            _driver = null;
        }
    }
}
