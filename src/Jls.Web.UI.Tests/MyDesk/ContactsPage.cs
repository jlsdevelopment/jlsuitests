﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.MyDesk
{
    public class ContactsPage : MyDeskPageBase
    {
        public ContactsPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        protected override void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("search-input"), timeout ?? DefaultTimeout, "Contacts page did not load in time.");
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/contacts";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                LoadPage(Driver);
            }
        }
    }
}
