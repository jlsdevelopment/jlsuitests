﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.MyDesk
{
    public class ListingManagerPage : MyDeskPageBase
    {
        private ReadOnlyCollection<IWebElement> ListingRows
        {
            get { return Driver.FindElements(By.ClassName("listing-row")); }
        }

        public IEnumerable<ListingRow> Listings
        {
            get { return ListingRows.Select(a => new ListingRow(a)); }
        }

        public IWebElement IncludeSoldCheckbox
        {
            get { return Driver.FindElement(By.Id("showSoldsCheckbox")); }
        }

        public IWebElement IncludeOffMarketCheckbox
        {
            get { return Driver.FindElement(By.Id("showOffMarketCheckbox")); }
        }

        public ListingManagerPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("my-listings-container"), timeout ?? DefaultTimeout, "Listing Manager page did not load in time.");
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/listingsmanager";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                LoadPage(Driver);
            }
        }
    }

    public class ListingRow
    {
        private IWebElement _row;

        public ListingRow(IWebElement element)
        {
            _row = element;
        }

        public IWebElement ListingIcons
        {
            get { return _row.FindElement(By.ClassName("listing-icons")); }
        }

        public IWebElement MlsNumber
        {
            get { return _row.FindElement(By.ClassName("listing-mls-num")); }
        }

        public IWebElement ShortCodeNumber
        {
            get { return _row.FindElement(By.ClassName("listing-short-code")); }
        }

        public IWebElement StreetAddress
        {
            get { return _row.FindElement(By.ClassName("listing-street-address")); }
        }

        public IWebElement City
        {
            get { return _row.FindElement(By.ClassName("listing-city")); }
        }

        public IWebElement Price
        {
            get { return _row.FindElement(By.ClassName("listing-price")); }
        }

        public IWebElement OverrideLink
        {
            get { return _row.FindElement(By.ClassName("listing-override")); }
        }

        public IWebElement SetTeamLink
        {
            get { return _row.FindElement(By.ClassName("listing-team-set")); }
        }

        public IWebElement SellerReportLink
        {
            get { return _row.FindElement(By.ClassName("listing-seller-report")); }
        }
    }
}
