﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.MyDesk
{
    public class EmailsPage : MyDeskPageBase
    {
        public EmailsPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        protected override void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("my-emails-search-input"), timeout ?? DefaultTimeout, "Emails page did not load in time.");
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/emails";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                LoadPage(Driver);
            }
        }
    }
}
