﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.MyDesk
{
    public class AdminPage : MyDeskPageBase
    {
        public AdminPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        protected override void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("admin"), timeout ?? DefaultTimeout, "Admin page did not load in time.");
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/admin";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                LoadPage(Driver);
            }
        }
    }
}
