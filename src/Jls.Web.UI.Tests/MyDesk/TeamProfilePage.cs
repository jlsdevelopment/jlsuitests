﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.MyDesk
{
    public class TeamProfilePage : ProfilePage
    {
        public IWebElement AddMemberIcon
        {
            get { return Driver.FindElement(By.Id("add-team-member")); }
        }

        public IWebElement AddMemberType
        {
            get { return Driver.FindElement(By.CssSelector("#add-member-type-ddl > span")); }
        }

        public IWebElement MemberTypeDdlParent
        {
            get { return Driver.FindElement(By.Id("add-member-type_listbox")); }
        }
        public IWebElement MemberTypeBrokerOption
        {
            get { return MemberTypeDdlParent.FindElement(By.CssSelector("li:nth-child(1)")); }
        }

        public IWebElement MemberTypeAssistantOption
        {
            get { return MemberTypeDdlParent.FindElement(By.CssSelector("li:nth-child(2)")); }
        }

        public IWebElement AddBrokerElement
        {
            get { return Driver.FindElement(By.Id("team-broker-search-input")); }
        }

        public string BrokerSearchInput
        {
            get { return AddBrokerElement.Text; }
            set
            {
                AddBrokerElement.Clear();
                AddBrokerElement.SendKeys(value);
            }
        }

        public IWebElement AddAssistantElement
        {
            get { return Driver.FindElement(By.Id("team-assistant-search-input")); }
        }

        public string AssistantSearchInput
        {
            get { return AddAssistantElement.Text; }
            set
            {
                AddAssistantElement.Clear();
                AddAssistantElement.SendKeys(value);
            }
        }

        public IWebElement AddMemberSubmit
        {
            get { return Driver.FindElement(By.Id("add-member-btn")); }
        }

        public IWebElement AddMemberCancel
        {
            get { return Driver.FindElement(By.Id("cancel-add-btn")); }
        }

        private ReadOnlyCollection<IWebElement> TeamMemberRows
        {
            get { return Driver.FindElements(By.ClassName("team-members-row")); }
        }
        public IEnumerable<TeamMember> TeamMembers
        {
            get { return TeamMemberRows.Select(a => new TeamMember(a)); }
        }


        #region Constructor

        public TeamProfilePage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        #endregion


        protected override void WaitUntilReady(int? timeout = null)
        {
            base.WaitUntilReady(timeout);
            Driver.WaitUntilVisible(AddMemberIcon, DefaultTimeout);
        }
    }

    public class TeamMember
    {
        private IWebElement _row;

        internal TeamMember(IWebElement element)
        {
            _row = element;
        }

        public IWebElement MemberType
        {
            get { return _row.FindElement(By.ClassName("member-type-text")); }
        }

        public IWebElement MemberName
        {
            get { return _row.FindElement(By.ClassName("member-name-text")); }
        }

        public IWebElement MemberDelete
        {
            get { return _row.FindElement(By.ClassName("k-delete-button")); }
        }
    }
}
