﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Jls.Utility;
using Jls.Web.UI.TestFramework;
using Jls.Web.UI.Tests;
using Jls.Web.UI.Tests.MyDesk;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace UITests.MyDesk
{
    public abstract class MyDeskPageBase : PageBase
    {
        #region Login Elements

        protected const string UsernameId = "username";

        protected IWebElement UsernameElement
        {
            get { return Driver.FindElement(By.Id(UsernameId)); }
        }

        public string Username
        {
            get { return UsernameElement.Text; }
            set { UsernameElement.SendKeys(value); }
        }

        protected const string PasswordId = "password";
        protected IWebElement PasswordElement
        {
            get { return Driver.FindElement(By.Id(PasswordId)); }
        }

        public string Password
        {
            get { return PasswordElement.Text; }
            set { PasswordElement.SendKeys(value); }
        }

        public IWebElement SubmitButton
        {
            get { return Driver.FindElement(By.Id("submit_btn")); }
        }

        #endregion

        #region Nav Elements

        private const string NavSubmenuId = "jls-navigation-sidebar";
        private IWebElement NavSubmenu
        {
            get { return Driver.FindElement(By.Id(NavSubmenuId)); }
        }

        public IWebElement ContactsLink
        {
            get { return NavSubmenu.FindElement(By.PartialLinkText("Contacts")); }
        }

        public IWebElement EmailsLink
        {
            get { return NavSubmenu.FindElement(By.PartialLinkText("Emails")); }
        }

        public IWebElement CampaignsLink
        {
            get { return NavSubmenu.FindElement(By.PartialLinkText("Campaigns")); }
        }

        private const string NavSidebarId = "jls-navigation-sidebar";
        private IWebElement NavSideBar
        {
            get { return Driver.FindElement(By.Id(NavSidebarId)); }
        }

        public IWebElement DashboardLink
        {
            get { return NavSideBar.FindElement(By.PartialLinkText("Dashboard")); }
        }

        public IWebElement ListingManagerLink
        {
            get { return NavSideBar.FindElement(By.PartialLinkText("Listing Manager")); }
        }

        public IWebElement MediaLibraryLink
        {
            get { return NavSideBar.FindElement(By.PartialLinkText("Media Library")); }
        }

        public IWebElement WebsiteDesignerLink
        {
            get { return NavSideBar.FindElement(By.PartialLinkText("Website Designer")); }
        }

        public IWebElement OutlookEmailLink
        {
            get { return NavSideBar.FindElement(By.PartialLinkText("Outlook Email")); }
        }

        public IWebElement OldMyDeskLink
        {
            get { return NavSideBar.FindElement(By.PartialLinkText("MyDesk (Old)")); }
        }

        #endregion

        #region Banner Elements

        protected IWebElement GlobalSearchInputElement
        {
            get { return Driver.FindElement(By.Id("global-search-input")); }
        }

        public string ImpersonationSearchInput
        {
            get { return GlobalSearchInputElement.Text; }
            set
            {
                GlobalSearchInputElement.Clear();
                GlobalSearchInputElement.SendKeys(value);
            }
        }

        public const string GlobalSearchSuggestionClassName = "impersonation-search-suggestions";
        public ReadOnlyCollection<IWebElement> ImpersonationSearchSuggestions
        {
            get { return Driver.FindElements(By.ClassName(GlobalSearchSuggestionClassName)); }
        }

        private const string UserMenuId = "user-menu";
        protected IWebElement UserMenuParent
        {
            get { return Driver.FindElement(By.Id(UserMenuId)); }
        }

        public IWebElement UserMenu
        {
            get { return UserMenuParent.FindElement(By.TagName("a")); }
        }

        private const string UserMenuContainerId = "md-user-menu";
        public IWebElement UserMenuDashboardLink
        {
            get { return UserMenuParent.FindElement(By.Id(UserMenuContainerId)).FindElement(By.PartialLinkText("Dashboard")); }
        }

        private const string DelegationLinkId = "md-menu-delegation";
        public IWebElement UserMenuDelegationLink
        {
            get { return UserMenuParent.FindElement(By.Id(DelegationLinkId)); }
        }

        private const string ProfileLinkId = "md-menu-profile";
        public IWebElement UserMenuProfileLink
        {
            get { return UserMenuParent.FindElement(By.Id(ProfileLinkId)); }
        }

        public IWebElement UserMenuLogoutLink
        {
            get { return UserMenuParent.FindElement(By.Id(UserMenuContainerId)).FindElement(By.PartialLinkText("Log Out")); }
        }

        private readonly By _logoutConfirmSelector = By.XPath("//*[@id='login_inner']/div[2]/div[2]/div/form/fieldset/div/button");
        public IWebElement LogoutConfirmButton
        {
            get { return Driver.FindElement(_logoutConfirmSelector); }
        }

        #endregion

        public const string AccessDeniedElementId = "accessDenied";
        public IWebElement AccessDeniedMessage
        {
            get { return Driver.FindElement(By.Id(AccessDeniedElementId)); }
        }

        public IWebElement ImpersonationProfileIcon
        {
            get { return Driver.FindElement(By.Id("impersonation-profile")); }
        }

        public IWebElement ExitImpersonationIcon
        {
            get { return Driver.FindElement(By.Id("impersonation-exit")); }
        }
        

        #region Constructors
        protected MyDeskPageBase() : this(null, true)
        {
        }

        protected MyDeskPageBase(bool waitToLoad) : this(null, waitToLoad)
        {
        }

        protected MyDeskPageBase(UserType? userType) : this(userType, true)
        {
        }

        protected MyDeskPageBase(UserType? userType, bool waitToLoad)
        {
            PageUrl = BaseUrl = AppConfig.MyDeskUrl();

            if (userType != null)
            {
                try
                {
                    // Try logging out first before logging in
                    Logout();
                }
                catch (Exception)
                {
                    // Ignore
                }
            }

            try
            {
                if (!Driver.Url.Contains(BaseUrl))
                {
                    LoadPage(Driver);
                    Driver.WaitUntilVisible(By.Id(UsernameId), DefaultTimeout);
                    Driver.WaitUntilDomReady();
                }
            }
            catch(WebDriverTimeoutException)
            {
                Driver.Navigate().Refresh();
                Driver.WaitUntilVisible(By.Id(UsernameId), DefaultTimeout);
                Driver.WaitUntilDomReady();
            }

            if (userType != null)
            {
                Login(userType.Value);
            }

            BrowseToPage();

            if (waitToLoad)
            {
                try
                {
                    WaitUntilReady();
                }
                catch (WebDriverTimeoutException)
                {
                    Driver.RefreshPage();
                    WaitUntilReady();
                }
            }

            Driver.WaitUntilDomReady();
        }
        #endregion

        protected void LoadPage(IWebDriver driver)
        {
            driver.Navigate().GoToUrl(PageUrl);
        }

        public void WaitUntilLoadingSpinnerDisappears()
        {
            var loadingSpinnerId = "crm-overlay";

            Driver.Pause();
            
            try
            {
                var loadingSpinner = Driver.FindElements(By.Id(loadingSpinnerId));
                if (loadingSpinner.IsNullOrEmpty() || !loadingSpinner.Any(a => a.Displayed))
                {
                    return;
                }
                Driver.WaitUntilVisible(loadingSpinner.First(a => a.Displayed), 2);
            }
            catch (WebDriverException)
            {
                // Just ignore if the 'loading' spinner does not get displayed
                return;
            }
            Driver.WaitUntilNotVisible(By.Id(loadingSpinnerId), DefaultTimeout, "MyDesk 'Loading' spinner did not disappear.");
        }

        public void Login(UserType userType)
        {
            Driver.WaitUntilVisible(UsernameElement, DefaultTimeout);

            switch (userType)
            {
                case UserType.Broker:
                    Username = AppConfig.MyDeskBrokerLogin();
                    Password = AppConfig.MyDeskBrokerPassword();
                    break;
                case UserType.TeamOwnerBroker:
                    Username = AppConfig.MyDeskTeamOwnerBrokerLogin();
                    Password = AppConfig.MyDeskTeamOwnerBrokerPassword();
                    break;
                case UserType.OfficeManager:
                    Username = AppConfig.MyDeskOfficeOwnerLogin();
                    Password = AppConfig.MyDeskOfficeOwnerPassword();
                    break;
                case UserType.CorporateStaff:
                    Username = AppConfig.MyDeskCorporateStaffLogin();
                    Password = AppConfig.MyDeskCorporateStaffPassword();
                    break;
                case UserType.CorporateManager:
                    Username = AppConfig.MyDeskCorporateManagerLogin();
                    Password = AppConfig.MyDeskCorporateManagerPassword();
                    break;
                case UserType.BrokerAssistant:
                    Username = AppConfig.MyDeskBrokerAssistantLogin();
                    Password = AppConfig.MyDeskBrokerAssistantPassword();
                    break;
                case UserType.CampaignManager:
                    Username = AppConfig.MyDeskCampaignManagerLogin();
                    Password = AppConfig.MyDeskCampaignManagerPassword();
                    break;
                case UserType.StandardUser:
                    Username = AppConfig.MyDeskStandardUserLogin();
                    Password = AppConfig.MyDeskStandardUserPassword();
                    break;
                case UserType.OfficeSupportStaff:
                    Username = AppConfig.MyDeskStandardUserLogin();
                    Password = AppConfig.MyDeskStandardUserPassword();
                    break;
            }

            try
            {
                SubmitButton.Submit();
            }
            catch (Exception)
            {
                Driver.Navigate().Refresh();
            }
        }

        public void Logout()
        {
            Driver.WaitUntilVisible(UserMenu, DefaultTimeout);

            UserMenu.ClickElement();
            UserMenuLogoutLink.ClickElement();

            Driver.WaitUntilVisible(_logoutConfirmSelector, DefaultTimeout);

            LogoutConfirmButton.Submit();
        }

        public void ClickDelegationLink()
        {
            ImpersonationSearchInput = "";
            Driver.WaitUntilVisible(By.Id(UserMenuId), DefaultTimeout);
            UserMenu.ClickElement();
            Driver.WaitUntilVisible(By.Id(DelegationLinkId), DefaultTimeout);
            UserMenuDelegationLink.ClickElement();
        }

        public void ClickProfileLink()
        {
            ImpersonationSearchInput = "";
            Driver.WaitUntilVisible(By.Id(UserMenuId), DefaultTimeout);
            UserMenu.ClickElement();
            Driver.WaitUntilVisible(By.Id(ProfileLinkId), DefaultTimeout);
            UserMenuProfileLink.ClickElement();
        }

        public void Impersonate(UserType user, string errorMsg)
        {
            ImpersonationSearchInput = user.EnumDescription();
            ClickFirstImpersonation();
            Driver.Pause();
            Driver.WaitUntilVisible(By.Id(MyDeskDashboard.EntityContextId), DefaultTimeout, errorMsg);
        }

        public void ClickFirstImpersonation()
        {
            if (!ImpersonationSearchSuggestions.Any())
            {
                return;
            }

            ClickFirstAutoSuggestion();
            Driver.WaitUntilVisible(By.Id(MyDeskDashboard.EntityContextId), DefaultTimeout);
            Driver.WaitUntilNotVisible(By.Id("crm-overlay"), DefaultTimeout);
        }

        public void ClickFirstAutoSuggestion()
        {
            Driver.Pause();
            ImpersonationSearchSuggestions.First().ClickElement();
        }
    }
}
