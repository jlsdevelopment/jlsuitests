﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.MyDesk
{
    public class ProfilePage : MyDeskPageBase
    {
        public IWebElement EditAccountIcon
        {
            get { return Driver.FindElement(By.Id("editAccount")); }
        }

        public IWebElement SaveProfile
        {
            get { return Driver.FindElement(By.Id("saveProfile")); }
        }

        public IWebElement CancelProfileChanges
        {
            get { return Driver.FindElement(By.Id("cancelProfile")); }
        }

        public ProfilePage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("account-fields"), timeout ?? DefaultTimeout, "Profile page did not load in time.");
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/profile";
            if (!Driver.Url.ToUpper().Contains(PageUrl.ToUpper()))
            {
                LoadPage(Driver);
            }
        }
    }
}
