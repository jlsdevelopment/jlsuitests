﻿using System.ComponentModel;

namespace Jls.Web.UI.Tests.MyDesk
{
    public enum UserType
    {
        [Description("uitest broker")]
        Broker,
        [Description("uitest officeowner")]
        OfficeManager,
        [Description("uitest staff")]
        CorporateStaff,
        [Description("uitest manager")]
        CorporateManager,
        [Description("uitest assistant")]
        BrokerAssistant,
        [Description("test office")]
        Office,
        [Description("uitest team")]
        Team,
        [Description("uitest anotherb")]
        TeamOwnerBroker,
        [Description("uitest campaign")]
        CampaignManager,
        [Description("uitest standard")]
        StandardUser,
        [Description("uitest support")]
        OfficeSupportStaff
    }
}
