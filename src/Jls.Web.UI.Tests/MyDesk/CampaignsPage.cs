﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.MyDesk
{
    public class CampaignsPage : MyDeskPageBase
    {
        public CampaignsPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        protected override void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("search-input"), timeout ?? DefaultTimeout, "Campaigns page did not load in time.");
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/campaigns";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                LoadPage(Driver);
            }
        }
    }
}
