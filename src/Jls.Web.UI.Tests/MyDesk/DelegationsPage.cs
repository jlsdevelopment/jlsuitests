﻿using System;
using System.Collections.ObjectModel;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.MyDesk
{
    public class DelegationsPage : MyDeskPageBase
    {
        public DelegationsPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        #region Page elements

        public const string AddButtonId = "add-delegate";
        public IWebElement AddButton
        {
            get { return Driver.FindElement(By.Id(AddButtonId)); }
        }

        public IWebElement NextButton
        {
            get { return Driver.FindElement(By.Id("wizardModalFooterNextButton")); }
        }

        public const string SaveButtonId = "wizardModalFooterSaveButton";
        public IWebElement SaveButton
        {
            get { return Driver.FindElement(By.Id(SaveButtonId)); }
        }

        public const string BrokerSearchInputId = "delegate-broker-search-input";
        protected IWebElement BrokerSearchElement
        {
            get { return Driver.FindElement(By.Id(BrokerSearchInputId)); }
        }
        public string BrokerSearch
        {
            get { return BrokerSearchElement.Text; }
            set { BrokerSearchElement.SendKeys(value);}
        }

        public ReadOnlyCollection<IWebElement> BrokerSearchSuggestions
        {
            get { return Driver.FindElements(By.ClassName("autocomplete-suggestion")); }
        }

        public const string CheckAllCssSelector = "#wizardModalBody > div > div > div > div > div > div.crm-list-wrapper.crm-list-wrapper > div > div.col-xs-2 > div > div > div > label";
        public IWebElement CheckAllCheckbox
        {
            get { return Driver.FindElement(By.CssSelector(CheckAllCssSelector)); }
        }

        public IWebElement MyDeskUserProfileCheckbox
        {
            get { return Driver.FindElement(By.CssSelector(".MyDesk.User.Profile")); }
        }

        public IWebElement MyDeskUserOfficeManagementCheckbox
        {
            get { return Driver.FindElement(By.CssSelector(".Office.Management")); }
        }

        public IWebElement MyDeskUserCompanyManagementCheckbox
        {
            get { return Driver.FindElement(By.CssSelector(".Company.Management")); }
        }

        public ReadOnlyCollection<IWebElement> ResourceTypesToDelegate
        {
            get { return Driver.FindElements(By.ClassName("delegation-resource-types")); }
        }

        public const string TargetDelegationsClassName = "target-delegations";

        public ReadOnlyCollection<IWebElement> TargetDelegations
        {
            get { return Driver.FindElements(By.ClassName(TargetDelegationsClassName)); }
        }

        public ReadOnlyCollection<IWebElement> TargetDelegationUserNames
        {
            get { return Driver.FindElements(By.ClassName("target-delegation-user-name")); }
        }

        public ReadOnlyCollection<IWebElement> TargetDelegationsResourceTypes
        {
            get { return Driver.FindElements(By.ClassName("target-delegation-resource-type")); }
        }

        public ReadOnlyCollection<IWebElement> EditDelegationButtons
        {
            get { return Driver.FindElements(By.ClassName("edit-delegation")); }
        }

        #endregion

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id(AddButtonId), timeout ?? DefaultTimeout, "Delegation page did not load in time.");
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/delegation";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                LoadPage(Driver);
            }
        }
    }
}
