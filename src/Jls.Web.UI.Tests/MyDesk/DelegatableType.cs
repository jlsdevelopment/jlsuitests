﻿using System.ComponentModel;

namespace Jls.Web.UI.Tests.MyDesk
{
    public enum DelegatableType
    {
        [Description("Contacts")]
        Contacts,
        [Description("CRM Preferences")]
        Preferences,
        [Description("Property Tracker Clients")]
        PropertyTrackerClients,
        [Description("Listings")]
        ListingManager,
        [Description("Media Library")]
        MediaLibrary,
        [Description("Web Site")]
        WebsiteDesigner,
        [Description("MyDesk User Profile")]
        MyDeskUserProfile,
        [Description("Email Library")]
        EmailLibrary,
        [Description("Listing Override")]
        ListingOverride,
        [Description("Campaign Library")]
        CampaignLibrary
    }
}
