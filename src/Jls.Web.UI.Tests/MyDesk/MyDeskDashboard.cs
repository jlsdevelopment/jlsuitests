﻿using System;
using System.Collections.Generic;
using Jls.Web.UI.Tests.MyDesk;
using OpenQA.Selenium;

namespace UITests.MyDesk
{
    public class MyDeskDashboard : MyDeskPageBase
    {
        public MyDeskDashboard(UserType userType, bool waitToLoad = true) : base(userType, waitToLoad)
        {
        }

        #region Elements

        public const string EntityContextId = "entity-context";

        #endregion

        override protected void WaitUntilReady(int? timeout = null)
        {
            WaitUntilLoadingSpinnerDisappears();
        }

        protected override void BrowseToPage()
        {
            if (!Driver.Url.Equals($"{BaseUrl}/", StringComparison.InvariantCultureIgnoreCase))
            {
                LoadPage(Driver);
            }
        }
    }
}
