﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.MyDesk
{
    public class PTClientsPage : MyDeskPageBase
    {
        public PTClientsPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("pt-client-pager"), timeout ?? DefaultTimeout, "Property Tracker Clients page did not load in time.");
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/propertytracker";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                LoadPage(Driver);
            }
        }
    }
}
