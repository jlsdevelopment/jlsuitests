﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.MyDesk
{
    public class MediaLibraryPage : MyDeskPageBase
    {
        public MediaLibraryPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("ckfinder-widget"), timeout ?? DefaultTimeout, "Media Library page did not load in time.");
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/medialibrary";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                LoadPage(Driver);
            }
        }
    }
}
