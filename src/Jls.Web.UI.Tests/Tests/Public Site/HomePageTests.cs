﻿using System;
using Jls.Web.UI.TestFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using UITests;
using UITests.Public_Site;

namespace Jls.Web.UI.Tests.Tests.Public_Site
{
    [TestClass]
    public class HomePageTests : TestBase
    {
        [TestMethod]
        public void TestProdRedirect()
        {
            Driver.Navigate().GoToUrl("http://johnlscott.com");

            try
            {
                Driver.WaitUntilVisible(By.Id("home-search-links"), AppConfig.Timeout());
            }
            catch (Exception)
            { 
                throw new WebDriverTimeoutException("Prod homepage timed out trying to redirect to 'www'.");
            }

            Assert.IsTrue(Driver.Url.StartsWith("https://www."));
        }

        [TestMethod]
        public void TestHyperlinks()
        {
            var homePage = new HomePage();

            Driver.OpenInNewTab(homePage.BuyingLink);
            new BuyingPage();
            Driver.CloseCurrentTab();

            Driver.OpenInNewTab(homePage.SellingLink);
            new SellingPage();
            Driver.CloseCurrentTab();

            Driver.OpenInNewTab(homePage.SearchHomesLink);
            new SearchPage();
            Driver.CloseCurrentTab();

            Driver.OpenInNewTab(homePage.SellTabLink);
            new SellingPage();
            Driver.CloseCurrentTab();

            Driver.OpenInNewTab(homePage.RelocateTabLink);
            new RelocationPage();
            Driver.CloseCurrentTab();

            homePage.AboutMenu.ClickElement();
            Driver.OpenInNewTab(homePage.OurCompanyLink);
            new CompanyPage();
            Driver.CloseCurrentTab();

            homePage.AboutMenu.ClickElement();
            Driver.OpenInNewTab(homePage.RelocationServicesLink);
            new RelocationPage();
            Driver.CloseCurrentTab();

            homePage.JoinUsMenu.ClickElement();
            Driver.OpenInNewTab(homePage.CareersLink);
            new CareersPage();
            Driver.CloseCurrentTab();

            homePage.JoinUsMenu.ClickElement();
            Driver.OpenInNewTab(homePage.FranchiseOpportunitiesLink);
            new FranchisePage();
            Driver.CloseCurrentTab();

            Driver.OpenInNewTab(homePage.ContactUsLink);
            new ContactPage();
            Driver.CloseCurrentTab();

            Driver.OpenInNewTab(homePage.FindAgentOfficeLink);
            new LocatorPage();
            Driver.CloseCurrentTab();

            //Clickable State Searches
            //for (var i = 0; i < homePage.ClickableStateSearches.Count(); i++)
            //{
            //    homePage.ClickableStateSearches.ToArray()[i].ClickElement();
            //    new SearchPage();
            //    Driver.Navigate().Back();
            //}
        }
    }
}
