﻿using System.Linq;
using Jls.Web.UI.TestFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using UITests;
using UITests.Public_Site;
using UITests.Public_Site.PropertyTracker;

namespace Jls.Web.UI.Tests.Tests.Public_Site
{
    [TestClass]
    public class PropertyTrackerTests : TestBase
    {
        [TestMethod]
        public void Favorites()
        {
            var ptProfilePage = new PTProfilePage();

            ptProfilePage.PTUserMenu.ClickElement();
            ptProfilePage.FavoritesLink.ClickElement();
            Driver.WaitUntilVisible(By.Id(PTProfilePage.RecentlyViewdId), DefaultTimeout);

            var searchPage = new SearchPage();
            searchPage.SearchInput = "kenmore";
       
            Driver.WaitUntilVisible(By.ClassName(SearchPage.AutoCompleteClassname), DefaultTimeout);
            Driver.Pause();
            var firstAutoSuggestion = searchPage.AutoCompleteSuggestions.First();
            firstAutoSuggestion.ClickElement();

            Driver.WaitUntilVisible(By.ClassName(SearchPage.ListingRowClass), DefaultTimeout);
            Driver.Pause(2000);
            var fav = searchPage.ListingsFavoriteIcons.First();
            fav.ClickElement();

            if (searchPage.BootboxModals.Any())
            {
                var ok = searchPage.BootboxModals
                                   .First()
                                   .FindElement(By.CssSelector("button[data-bb-handler='confirm']"));
                ok.ClickElement();
            }

            ptProfilePage.Logoff();
        }

        [TestMethod]
        public void Searches()
        {
            var ptProfilePage = new PTProfilePage();

            ptProfilePage.PTUserMenu.ClickElement();
            ptProfilePage.SearchesLink.ClickElement();
            Driver.WaitUntilVisible(By.Id(PTProfilePage.SearchesTabId), DefaultTimeout);

            ptProfilePage.Logoff();
        }

        [TestMethod]
        public void Profile()
        {
            var ptProfilePage = new PTProfilePage();

            ptProfilePage.PTUserMenu.ClickElement();
            ptProfilePage.ProfileLink.ClickElement();
            Driver.WaitUntilVisible(By.Id(PTProfilePage.ProfileTabId), DefaultTimeout);

            ptProfilePage.Logoff();
        }

        [TestMethod]
        public void Settings()
        {
            var ptProfilePage = new PTProfilePage();

            ptProfilePage.PTUserMenu.ClickElement();
            ptProfilePage.SettingsLink.ClickElement();
            Driver.WaitUntilVisible(By.Id(PTProfilePage.SettingsTabId), DefaultTimeout);

            ptProfilePage.Logoff();
        }

    }
}
