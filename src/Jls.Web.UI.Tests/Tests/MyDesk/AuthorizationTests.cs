﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using Jls.Web.UI.Tests.MyDesk;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UITests;
using UITests.MyDesk;
using Jls.Utility;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace Jls.Web.UI.Tests.Tests.MyDesk
{
    [TestClass]
    public class AuthorizationTests : TestBase
    {
        /********************************************
        * Data must be setup manually as follows:
        * Broker and Office Owner belong to the same office (Bellevue-Issaquah)
        * Corporate Staff belongs to a different office
        * Corporate Manager belongs to Bellevue Main office
        * Broker has a LAG ID from a broker that has listings
        ********************************************/


        [TestMethod]
        public void OutlookLinkOpensInNewTab()
        {
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            ValidateOutlookLink(myDesk);

            Driver.Pause();
            myDesk.Logout();
        }

        [TestMethod]
        public void AdminPageAccess()
        {
            // Cannot access
            new MyDeskDashboard(UserType.StandardUser);
            ValidateAccessDenied(new AdminPage());

            new MyDeskDashboard(UserType.BrokerAssistant);
            ValidateAccessDenied(new AdminPage());

            new MyDeskDashboard(UserType.Broker);
            ValidateAccessDenied(new AdminPage());

            new MyDeskDashboard(UserType.OfficeSupportStaff);
            ValidateAccessDenied(new AdminPage());

            new MyDeskDashboard(UserType.CorporateStaff);
            ValidateAccessDenied(new AdminPage());

            // Can access
            new MyDeskDashboard(UserType.CorporateManager);
            new AdminPage();

            new MyDeskDashboard(UserType.OfficeManager);
            new AdminPage();
        }

        #region Individual Delegations
        [TestMethod]
        public void UserProfileDelegation()
        {
            ClearUserDelegations(UserType.Broker);

            // Check and make sure Broker Asst user can't impersonate Broker user
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsFalse(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.Logout();

            // Corporate Staff user delegates "MyDesk User Profile" to Broker user
            DelegateType(UserType.Broker, UserType.BrokerAssistant, new List<DelegatableType> { DelegatableType.MyDeskUserProfile });

            // Log in as Broker Asst user and impersonate Broker user
            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout,
                "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            var profilePage = new ProfilePage();
            ValidateStandardUserRestrictedMenuItems(profilePage);
            ValidateStandardUserRestrictedPages();

            profilePage.Logout();
            ClearUserDelegations(UserType.Broker);
        }

        [TestMethod]
        public void ContactsDelegation()
        {
            ClearUserDelegations(UserType.Broker);

            // Check and make sure Broker Asst user can't impersonate Broker user
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsFalse(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.Logout();

            DelegateType(UserType.Broker, UserType.BrokerAssistant, new List<DelegatableType> { DelegatableType.Contacts });

            // Log in as Broker Asst user and impersonate Broker user
            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout,
                "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            myDesk.ContactsLink.ClickElement();
            var contactsPage = new ContactsPage();

            contactsPage.Logout();
            ClearUserDelegations(UserType.Broker);
        }

        [TestMethod]
        public void PreferencesDelegation()
        {
            ClearUserDelegations(UserType.Broker);

            // Check and make sure Broker Asst user can't impersonate Broker user
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsFalse(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.Logout();

            DelegateType(UserType.Broker, UserType.BrokerAssistant, new List<DelegatableType> { DelegatableType.Preferences });

            // Log in as Broker Asst user and impersonate Broker user
            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout,
                "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            myDesk.Logout();
            ClearUserDelegations(UserType.Broker);
        }

        [TestMethod]
        public void PropertyTrackerClientsDelegation()
        {
            ClearUserDelegations(UserType.Broker);

            // Check and make sure Broker Asst user can't impersonate Broker user
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsFalse(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.Logout();

            DelegateType(UserType.Broker, UserType.BrokerAssistant, new List<DelegatableType> { DelegatableType.PropertyTrackerClients });

            // Log in as Broker Asst user and impersonate Broker user
            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout,
                "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            myDesk.Logout();
            ClearUserDelegations(UserType.Broker);
        }

        [TestMethod]
        public void ListingManagerOverrideDelegation()
        {
            ClearUserDelegations(UserType.Broker);

            // Check and make sure Broker Asst user can't impersonate Broker user
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsFalse(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.Logout();

            DelegateType(UserType.Broker, UserType.BrokerAssistant, new List<DelegatableType> { DelegatableType.ListingManager });

            // Log in as Broker Asst user and impersonate Broker user
            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout,
                "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            Assert.IsFalse(myDesk.ContactsLink.IsClickable());
            Assert.IsFalse(myDesk.MediaLibraryLink.IsClickable());
            Assert.IsFalse(myDesk.WebsiteDesignerLink.IsClickable());

            myDesk.ListingManagerLink.ClickElement();
            var listingsPage = new ListingManagerPage();

            Assert.IsFalse(Driver.FindElements(By.ClassName("listing-override")).Any());

            listingsPage.Logout();

            // Delegate Listing Override this time
            DelegateType(UserType.Broker, UserType.BrokerAssistant, new List<DelegatableType> { DelegatableType.ListingManager, DelegatableType.ListingOverride });

            // Log in as Broker Asst user and impersonate Broker user
            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout,
                "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            Assert.IsFalse(myDesk.ContactsLink.IsClickable());
            Assert.IsFalse(myDesk.MediaLibraryLink.IsClickable());
            Assert.IsFalse(myDesk.WebsiteDesignerLink.IsClickable());

            myDesk.ListingManagerLink.ClickElement();
            listingsPage = new ListingManagerPage();

            Assert.IsTrue(listingsPage.Listings.Any(a => a.OverrideLink.IsClickable()));

            listingsPage.Logout();

            ClearUserDelegations(UserType.Broker);
        }

        [TestMethod]
        public void MediaLibraryDelegation()
        {
            ClearUserDelegations(UserType.Broker);

            // Check and make sure Broker Asst user can't impersonate Broker user
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsFalse(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.Logout();

            DelegateType(UserType.Broker, UserType.BrokerAssistant, new List<DelegatableType> { DelegatableType.MediaLibrary });

            // Log in as Broker Asst user and impersonate Broker user
            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout,
                "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            myDesk.MediaLibraryLink.ClickElement();
            var mediaPage = new MediaLibraryPage();

            mediaPage.Logout();
            ClearUserDelegations(UserType.Broker);
        }

        [TestMethod]
        public void WebsiteDesignerDelegation()
        {
            ClearUserDelegations(UserType.Broker);

            // Check and make sure Broker Asst user can't impersonate Broker user
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsFalse(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.Logout();

            DelegateType(UserType.Broker, UserType.BrokerAssistant, new List<DelegatableType> { DelegatableType.WebsiteDesigner });

            // Log in as Broker Asst user and impersonate Broker user
            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout,
                "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            myDesk.WebsiteDesignerLink.ClickElement();
            var siteBuilderPage = new WebsiteDesignerPage();

            Assert.IsFalse(siteBuilderPage.ContactsLink.IsClickable());
            Assert.IsFalse(siteBuilderPage.ListingManagerLink.IsClickable());
            Assert.IsFalse(siteBuilderPage.MediaLibraryLink.IsClickable());

            myDesk.Logout();
            ClearUserDelegations(UserType.Broker);
        }
        #endregion

        #region Standard User
        [TestMethod]
        public void StandardUserCanAccessCorrectFeatures()
        {
            var myDesk = new MyDeskDashboard(UserType.StandardUser);
            ValidateMinimumAccess(myDesk);
            myDesk.Logout();
        }

        [TestMethod]
        public void StandardUserCanImpersonateUsersThruDelegationOnly()
        {
            ClearUserDelegations(UserType.Broker);

            // Check and make sure Broker Asst user can't impersonate Broker user
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsFalse(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.Logout();

            // Delegate "MyDesk User Profile" to standard user
            DelegateType(UserType.Broker, UserType.StandardUser, new List<DelegatableType> { DelegatableType.MyDeskUserProfile });

            // Log in and impersonate Broker user
            myDesk = new MyDeskDashboard(UserType.StandardUser);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout, "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            var profilePage = new ProfilePage();
            ValidateStandardUserRestrictedMenuItems(profilePage);
            ValidateStandardUserRestrictedPages();

            myDesk.Logout();

            // Delegate Property Tracker Clients
            DelegateType(UserType.Broker, UserType.BrokerAssistant, new List<DelegatableType> { DelegatableType.ListingManager });

            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout, "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            ValidateDisabledLink(myDesk.ContactsLink);
            ValidateDisabledLink(myDesk.MediaLibraryLink);

            Driver.Pause();

            myDesk.ListingManagerLink.ClickElement();
            var listingsPage = new ListingManagerPage();

            listingsPage.Logout();

            ClearUserDelegations(UserType.Broker);
        }
        #endregion

        #region Broker Assistant
        /*
         * Given that I am a MyDesk user of type "Broker"
           when I search for other users to impersonate in MyDesk
           then I only see users that have delegated their permissions to me

         ** Applies to all user types except Brokercare, Corporate Manager, and Developer
         */
        [TestMethod]
        public void BrokerAssistantCanImpersonateUsersThruDelegationOnly()
        {
            ClearUserDelegations(UserType.Broker);

            // Check and make sure Broker Asst user can't impersonate Broker user
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsFalse(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.Logout();

            // Dlegate "MyDesk User Profile" to Broker Assistant user
            DelegateType(UserType.Broker, UserType.BrokerAssistant, new List<DelegatableType> { DelegatableType.MyDeskUserProfile });

            // Log in and impersonate Broker user
            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout, "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            var profilePage = new ProfilePage();
            ValidateStandardUserRestrictedMenuItems(profilePage);
            ValidateStandardUserRestrictedPages();

            myDesk.Logout();

            // Delegate Property Tracker Clients
            DelegateType(UserType.Broker, UserType.BrokerAssistant, new List<DelegatableType> { DelegatableType.ListingManager });

            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();

            Driver.WaitUntilVisible(By.ClassName(MyDeskPageBase.GlobalSearchSuggestionClassName), DefaultTimeout, "There was no search result displayed.");
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.First().Text.Contains(UserType.Broker.EnumDescription()));

            myDesk.ClickFirstImpersonation();

            ValidateDisabledLink(myDesk.ContactsLink);
            ValidateDisabledLink(myDesk.MediaLibraryLink);

            Driver.Pause();

            myDesk.ListingManagerLink.ClickElement();
            var listingsPage = new ListingManagerPage();

            listingsPage.Logout();

            ClearUserDelegations(UserType.Broker);
        }

        [TestMethod]
        public void BrokerAssistantCanAccessCorrectFeatures()
        {
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            ValidateBrokerAssistantAccess(myDesk, false);
            myDesk.Logout();
        }
        #endregion

        [TestMethod]
        public void BrokerCanAccessAllFeatures()
        {
            var myDesk = new MyDeskDashboard(UserType.Broker);
            ValidateBrokerAccess(myDesk, false);
            myDesk.Logout();
        }

        [Ignore]
        [TestMethod]
        public void ResourceAvailabilityFollowsPersonaAssigned()
        {
            var myDesk = new MyDeskDashboard(UserType.Broker);
            ValidateBrokerAccess(myDesk, false);

            myDesk.ClickProfileLink();

            myDesk.Logout();
        }

        #region Office Manager
        /*
         * Given that I am a MyDesk user of type "Office Owner"
           when I search for other users to impersonate in MyDesk
           then I see other users in the same office as myself
           and I see users that have delegated their permissions to me

        ** The same applies to Corporate Staff users
        */
        [TestMethod]
        public void OfficeManagerCanImpersonateOfficeUsersWithoutDelegation()
        {
            ClearUserDelegations(UserType.Broker);
            ClearUserDelegations(UserType.BrokerAssistant);

            // Check that broker has nothing delegated to the Office Owner user
            var myDesk = new MyDeskDashboard(UserType.Broker);
            myDesk.ClickDelegationLink();

            var delegationsPage = new DelegationsPage();
            Assert.IsFalse(delegationsPage.TargetDelegationUserNames.Any(a => a.Text.Equals(UserType.OfficeManager.EnumDescription(), StringComparison.InvariantCultureIgnoreCase)));

            myDesk.Logout();

            // Office Owner cannot impersonate anyone from a different office without delegation
            myDesk = new MyDeskDashboard(UserType.OfficeManager);
            myDesk.ImpersonationSearchInput = UserType.BrokerAssistant.EnumDescription();
            Driver.Pause();
            Assert.IsFalse(myDesk.ImpersonationSearchSuggestions.Any());

            // Office Owner can impersonate Broker user because they belong to the same office
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.ClickFirstImpersonation();

            // Office Owner cannot access Contacts/PT Clients without specific delegation
            ValidateDisabledLink(myDesk.ContactsLink);

            Assert.IsTrue(myDesk.MediaLibraryLink.IsClickable());
            Assert.IsTrue(myDesk.ListingManagerLink.IsClickable());
            Assert.IsTrue(myDesk.WebsiteDesignerLink.IsClickable());

            myDesk.ImpersonationSearchInput = "";
            myDesk.Logout();
        }

        [TestMethod]
        public void OfficeManagerCanAccessContactsPTClientsDuringImpersonationThruDelegationOnly()
        {
            ClearUserDelegations(UserType.Broker);

            // Office Owner can impersonate Broker user because they belong to the same office
            var myDesk = new MyDeskDashboard(UserType.OfficeManager);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.ClickFirstImpersonation();
            myDesk.Logout();

            // Delegate Website
            DelegateType(UserType.Broker, UserType.OfficeManager, new List<DelegatableType> { DelegatableType.WebsiteDesigner });

            myDesk = new MyDeskDashboard(UserType.OfficeManager);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.ClickFirstImpersonation();

            myDesk.WebsiteDesignerLink.ClickElement();
            new WebsiteDesignerPage();

            myDesk.Logout();

            // Property Tracker Clients delegation
            DelegateType(UserType.Broker, UserType.OfficeManager, new List<DelegatableType> { DelegatableType.PropertyTrackerClients, DelegatableType.Contacts });

            // Office Owner cannot impersonate anyone from a different office without delegation
            myDesk = new MyDeskDashboard(UserType.OfficeManager);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.ClickFirstImpersonation();

            myDesk.ContactsLink.ClickElement();
            var contactsPage = new ContactsPage();

            contactsPage.Logout();

            ClearUserDelegations(UserType.Broker);
        }

        [TestMethod]
        public void OfficeManagerCanAccessAuthorizedFeaturesOnly()
        {
            var myDesk = new MyDeskDashboard(UserType.OfficeManager);
            ValidateOfficeManagerAccess(myDesk, false);
            myDesk.Logout();
        }

        [Ignore]
        [TestMethod]
        public void OfficeOwnerHasListingManagerPTClientsOnlyWhenAlsoABroker()
        {
            var myDesk = new MyDeskDashboard(UserType.OfficeManager);
            myDesk.ClickProfileLink();
            var profilePage = new ProfilePage();

            profilePage.Logout();

            // Delegate Website
            DelegateType(UserType.Broker, UserType.OfficeManager, new List<DelegatableType> { DelegatableType.WebsiteDesigner });

            myDesk = new MyDeskDashboard(UserType.OfficeManager);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.ClickFirstImpersonation();

            myDesk.WebsiteDesignerLink.ClickElement();
            new WebsiteDesignerPage();

            myDesk.Logout();

            // Property Tracker Clients delegation
            DelegateType(UserType.Broker, UserType.OfficeManager, new List<DelegatableType> { DelegatableType.PropertyTrackerClients, DelegatableType.Contacts });

            // Office Owner cannot impersonate anyone from a different office without delegation
            myDesk = new MyDeskDashboard(UserType.OfficeManager);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.ClickFirstImpersonation();

            myDesk.ContactsLink.ClickElement();
            var contactsPage = new ContactsPage();

            contactsPage.Logout();

            ClearUserDelegations(UserType.Broker);
        }
        #endregion

        #region Corporate Staff
        [TestMethod]
        public void CorporateStaffCanImpersonateAnyoneFromAnyOfficeWithoutDelegation()
        {
            ClearUserDelegations(UserType.BrokerAssistant);
            ClearUserDelegations(UserType.Broker);

            // Check that Broker Assistant has nothing delegated to the Corporate Staff user
            var myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.ClickDelegationLink();

            var delegationsPage = new DelegationsPage();
            Assert.IsFalse(delegationsPage.TargetDelegationUserNames.Any(a => a.Text.Equals(UserType.CorporateStaff.EnumDescription(), StringComparison.InvariantCultureIgnoreCase)));

            myDesk.Logout();

            // Corporate Staff can impersonate anyone from a different office without delegation
            myDesk = new MyDeskDashboard(UserType.CorporateStaff);
            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any(), "Corporate Staff user is unable to impersonate users in other offices.");

            // Corporate Staff can impersonate Broker Assistant user because they belong to the same office
            myDesk.ImpersonationSearchInput = UserType.BrokerAssistant.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any(), "Corporate Staff is unable to impersonate users from the same office.");

            myDesk.ClickFirstImpersonation();

            myDesk.ImpersonationSearchInput = "";
            myDesk.Logout();
        }

        [TestMethod]
        public void CorporateStaffCanAccessContactsPTClientsThruDelegationOnly()
        {
            ClearUserDelegations(UserType.TeamOwnerBroker);

            // Corp Staff can impersonate Broker user because they belong to the same office
            var myDesk = new MyDeskDashboard(UserType.CorporateStaff);
            myDesk.ImpersonationSearchInput = UserType.TeamOwnerBroker.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.ClickFirstImpersonation();
            myDesk.Logout();

            // Property Tracker Clients delegation
            DelegateType(UserType.TeamOwnerBroker, UserType.CorporateStaff, new List<DelegatableType> { DelegatableType.PropertyTrackerClients, DelegatableType.Contacts });

            // Office Owner cannot impersonate anyone from a different office without delegation
            myDesk = new MyDeskDashboard(UserType.CorporateStaff);
            myDesk.ImpersonationSearchInput = UserType.TeamOwnerBroker.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any());

            myDesk.ClickFirstImpersonation();

            myDesk.ContactsLink.ClickElement();
            var contactsPage = new ContactsPage();

            contactsPage.Logout();

            ClearUserDelegations(UserType.TeamOwnerBroker);
        }
        #endregion

        /*
         * Given that I am a MyDesk user of type "Corporate Manager"
           when I search for other users to impersonate in MyDesk
           then I see all users
        */
        [TestMethod]
        public void CorporateManagerCanImpersonateAnyoneFromAnyOfficeWithoutDelegation()
        {
            var myDesk = new MyDeskDashboard(UserType.CorporateManager);

            ValidateCorpManagerAccess(myDesk);

            myDesk.ImpersonationSearchInput = UserType.OfficeManager.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any(), "There was no auto-complete suggestion to impersonate an Office Owner.");
            myDesk.ClickFirstImpersonation();

            ValidateOfficeManagerAccess(myDesk, true);

            myDesk.ImpersonationSearchInput = UserType.CorporateStaff.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any(), "There was no auto-complete suggestion to impersonate a Corporate Staff.");
            myDesk.ClickFirstImpersonation();

            ValidateCorpStaffAccess(myDesk, true);

            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any(), "There was no auto-complete suggestion to impersonate a Broker.");
            myDesk.ClickFirstImpersonation();

            ValidateBrokerAccess(myDesk, true);

            myDesk.ImpersonationSearchInput = UserType.Office.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any(), "There was no auto-complete suggestion to impersonate an Office.");
            myDesk.ClickFirstImpersonation();

            myDesk.ImpersonationSearchInput = UserType.Team.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any(), "There was no auto-complete suggestion to impersonate a Team.");

            myDesk.ImpersonationSearchInput = "Mike Hanney";
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any(), "There was no auto-complete suggestion to impersonate Mike Hanney.");
            myDesk.ClickFirstImpersonation();

            myDesk.ImpersonationSearchInput = "maria evangelista";
            Driver.Pause();
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any(), "There was no auto-complete suggestion to impersonate Maria Evangelista.");
            myDesk.ClickFirstImpersonation();

            myDesk.ImpersonationSearchInput = "";
            myDesk.Logout();
        }

        /*
         * Campaign Manager should also have the Corporate Staff persona
        */

        [TestMethod]
        public void CampaignManagerCanAccessBrokersCampaignWithoutDelegation()
        {
            ClearUserDelegations(UserType.Broker);

            // Office Owner can impersonate Broker user because they belong to the same office
            var myDesk = new MyDeskDashboard(UserType.CampaignManager);
            ValidateSupportStaffAccess(myDesk, false);

            myDesk.ImpersonationSearchInput = UserType.Broker.EnumDescription();
            Driver.Pause(1500);
            Assert.IsTrue(myDesk.ImpersonationSearchSuggestions.Any(), "CRM Campaign Manager was unable to search for and impersonate Broker.");

            myDesk.ClickFirstImpersonation();

            ValidateBrokerAccess(myDesk, true);

            myDesk.Logout();
        }


        #region Private methods

        private void ClearUserDelegations(UserType user)
        {
            var myDesk = new MyDeskDashboard(user);
            myDesk.ClickDelegationLink();

            var delegationsPage = new DelegationsPage();

            while (delegationsPage.EditDelegationButtons.Any())
            {
                var editButton = delegationsPage.EditDelegationButtons.First();

                editButton.ClickElement();
                Driver.WaitUntilVisible(By.CssSelector(DelegationsPage.CheckAllCssSelector), DefaultTimeout);

                delegationsPage.CheckAllCheckbox.ClickElement(); // this checks all checkboxes
                Driver.Pause();
                delegationsPage.CheckAllCheckbox.ClickElement(); // then uncheck all checkboxes

                delegationsPage.NextButton.ClickElement();
                Driver.WaitUntilVisible(By.Id(DelegationsPage.SaveButtonId), DefaultTimeout);

                Assert.IsTrue(delegationsPage.ResourceTypesToDelegate.Any(a => a.Text == "None"));

                delegationsPage.SaveButton.ClickElement();
                Driver.Pause();
                Driver.WaitUntilVisible(By.Id(DelegationsPage.AddButtonId), DefaultTimeout);
            }

            myDesk.Logout();
        }

        private void ValidateDisabledLink(IWebElement item, string errorMsg = null)
        {
            Assert.IsFalse(item.IsClickable(), errorMsg ?? $"Link element {item.Text}, expected to be disabled, was not disabled.");
        }

        private void ValidateAccessDenied(IWebElement item)
        {
            item.ClickElement();
            Driver.WaitUntilVisible(By.Id(MyDeskPageBase.AccessDeniedElementId), DefaultTimeout, $"Menu link '{nameof(item)}' did not display the 'Access Denied' message");
            Driver.Pause(500);
        }

        private void ValidateAccessDenied(MyDeskPageBase page)
        {
            Driver.WaitUntilVisible(By.Id(MyDeskPageBase.AccessDeniedElementId), DefaultTimeout, $"'{page.GetType().Name}' did not display the 'Access Denied' message");
            Driver.Pause(500);
        }

        private void ValidateStandardUserRestrictedMenuItems(MyDeskPageBase page)
        {
            ValidateDisabledLink(page.ContactsLink);
            ValidateDisabledLink(page.ListingManagerLink);
            ValidateDisabledLink(page.MediaLibraryLink);
            ValidateDisabledLink(page.WebsiteDesignerLink); 
        }

        private void ValidateStandardUserRestrictedPages()
        {
            ValidateAccessDenied(new ContactsPage(false));
            ValidateAccessDenied(new ListingManagerPage(false));
            ValidateAccessDenied(new MediaLibraryPage(false));
            ValidateAccessDenied(new PTClientsPage(false));
            ValidateAccessDenied(new WebsiteDesignerPage(false));
            ValidateAccessDenied(new PreferencesPage(false));
        }

        private void ValidateOutlookLink(MyDeskPageBase myDesk)
        {
            Assert.AreEqual(Driver.WindowHandles.Count, 1);
            myDesk.OutlookEmailLink.ClickElement();
            Assert.AreEqual(2, Driver.WindowHandles.Count);

            Driver.SwitchToLastTab();
            Driver.WaitUntilVisible(By.Id("username"), DefaultTimeout, "Outlook Web App (OWA) login screen elements may have changed.");
            Assert.IsTrue(Driver.Url.Contains("outlook.johnlscott.com"));

            Driver.CloseCurrentTab();
            Driver.SwitchToMainTab();
        }

        private void ValidateLegacyMyDeskLink(MyDeskPageBase myDesk)
        {
            Assert.AreEqual(Driver.WindowHandles.Count, 1);
            myDesk.OldMyDeskLink.ClickElement();
            Assert.AreEqual(Driver.WindowHandles.Count, 2);

            Driver.SwitchToLastTab();
            try
            {
                Driver.WaitUntilVisible(By.CssSelector("input[name=userid]"), DefaultTimeout, "Legacy MyDesk login screen did not load in time, or screen elements may have changed.");
            }
            catch (WebDriverTimeoutException)
            {
                Assert.IsTrue(Driver.Url.Contains("jlslegacy.com/jls"), $"The MyDesk Legacy link result in the following url: {Driver.Url}");
            }

            Driver.CloseCurrentTab();
            Driver.SwitchToMainTab();
        }

        private void ValidateMinimumAccess(MyDeskPageBase myDesk)
        {
            myDesk.ClickDelegationLink();
            var delegationPage = new DelegationsPage();

            delegationPage.ClickProfileLink();
            var profilePage = new ProfilePage();

            ValidateStandardUserRestrictedMenuItems(profilePage);
            ValidateStandardUserRestrictedPages();
        }

        private void ValidateBrokerAccess(MyDeskPageBase myDesk, bool isImpersonated)
        {
            ClickMenuItem(myDesk.ListingManagerLink);
            var listingsPage = new ListingManagerPage();

            ClickMenuItem(listingsPage.MediaLibraryLink);
            var mediaPage = new MediaLibraryPage();

            ClickMenuItem(mediaPage.WebsiteDesignerLink);
            var sitePage = new WebsiteDesignerPage();

            if (!isImpersonated)
            {
                ClickMenuItem(sitePage.ContactsLink);
                var contactsPage = new ContactsPage();

                contactsPage.ClickDelegationLink();
                var delegationsPage = new DelegationsPage();

                delegationsPage.ClickProfileLink();
                new ProfilePage();
            }
        }

        private void ValidateCorpManagerAccess(MyDeskPageBase myDesk)
        {
            ClickMenuItem(myDesk.ContactsLink);
            var contactsPage = new ContactsPage();

            ClickMenuItem(contactsPage.MediaLibraryLink);
            var mediaPage = new MediaLibraryPage();

            ClickMenuItem(mediaPage.WebsiteDesignerLink);
            var sitePage = new WebsiteDesignerPage();

            ValidateDisabledLink(sitePage.ListingManagerLink);

            sitePage.ClickDelegationLink();
            var delegationsPage = new DelegationsPage();

            delegationsPage.ClickProfileLink();
            new ProfilePage();

            new AdminPage();
        }

        private void ValidateCorpStaffAccess(MyDeskPageBase myDesk, bool isImpersonated)
        {
            ClickMenuItem(myDesk.ContactsLink);
            var contactsPage = new ContactsPage();

            ClickMenuItem(contactsPage.MediaLibraryLink);
            var mediaPage = new MediaLibraryPage();

            ClickMenuItem(mediaPage.WebsiteDesignerLink);
            var sitePage = new WebsiteDesignerPage();

            ValidateDisabledLink(sitePage.ListingManagerLink);

            if (!isImpersonated)
            {
                contactsPage.ClickDelegationLink();
                var delegationsPage = new DelegationsPage();

                delegationsPage.ClickProfileLink();
                new ProfilePage();
            }

            ValidateAccessDenied(new AdminPage());
        }

        private void ValidateOfficeManagerAccess(MyDeskPageBase myDesk, bool isImpersonated)
        {
            ClickMenuItem(myDesk.MediaLibraryLink);
            var mediaPage = new MediaLibraryPage();

            ClickMenuItem(mediaPage.WebsiteDesignerLink);
            var sitePage = new WebsiteDesignerPage();

            ClickMenuItem(sitePage.WebsiteDesignerLink);
            var contactsPage = new ContactsPage();

            ValidateDisabledLink(contactsPage.ListingManagerLink);

            if (!isImpersonated)
            {
                contactsPage.ClickDelegationLink();
                var delegationsPage = new DelegationsPage();

                delegationsPage.ClickProfileLink();
                new ProfilePage();
            }
        }

        private void ValidateBrokerAssistantAccess(MyDeskPageBase myDesk, bool isImpersonated)
        {
            ClickMenuItem(myDesk.ContactsLink);
            var contactsPage = new ContactsPage();

            ValidateDisabledLink(contactsPage.MediaLibraryLink);
            ValidateDisabledLink(contactsPage.ListingManagerLink);
            ValidateDisabledLink(contactsPage.WebsiteDesignerLink);

            if (!isImpersonated)
            {
                contactsPage.ClickDelegationLink();
                var delegationsPage = new DelegationsPage();

                delegationsPage.ClickProfileLink();
                new ProfilePage();
            }
        }

        private void ValidateSupportStaffAccess(MyDeskPageBase myDesk, bool isImpersonated)
        {
            ClickMenuItem(myDesk.MediaLibraryLink);
            var mediaPage = new MediaLibraryPage();

            ClickMenuItem(mediaPage.WebsiteDesignerLink);
            var sitePage = new WebsiteDesignerPage();

            ClickMenuItem(sitePage.WebsiteDesignerLink);
            var contactsPage = new ContactsPage();

            ValidateDisabledLink(contactsPage.ListingManagerLink);

            if (!isImpersonated)
            {
                contactsPage.ClickDelegationLink();
                var delegationsPage = new DelegationsPage();

                delegationsPage.ClickProfileLink();
                new ProfilePage();
            }
        }

        private void ClickMenuItem(IWebElement element)
        {
            Driver.Pause();
            Driver.WaitUntilClickable(element, DefaultTimeout, $"Element '{element.Text}' did not become clickable.");
            element.ClickElement();
        }

        #endregion
    }
}
