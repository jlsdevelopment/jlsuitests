﻿using System;
using System.Linq;
using Jls.Utility;
using Jls.Web.UI.TestFramework;
using Jls.Web.UI.Tests.MyDesk;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using UITests;
using UITests.MyDesk;
using UITests.Public_Site;

namespace Jls.Web.UI.Tests.Tests.MyDesk
{
    [TestClass]
    public class TeamTests : TestBase
    {
        [TestMethod]
        public void OnlyBrokerTeamMembersShowOnSearch()
        {
            // Add a broker as a team assistant
            var myDesk = new MyDeskDashboard(UserType.TeamOwnerBroker);

            myDesk.Impersonate(UserType.Team, "Team owner (broker) was unable to impersonate the team.");

            CleanupTeamMembership(myDesk);

            myDesk.ImpersonationProfileIcon.ClickElement();
            var profilePage = new TeamProfilePage();
            var membersCount = profilePage.TeamMembers.Count();

            Driver.ScrollToElement(profilePage.AddMemberIcon);
            profilePage.AddMemberIcon.ClickElement();

            Driver.Pause(300);

            profilePage.AddMemberType.ClickElement();
            profilePage.MemberTypeAssistantOption.ClickElement();
            Driver.WaitUntilVisible(profilePage.AddAssistantElement, DefaultTimeout);

            profilePage.AssistantSearchInput = UserType.Broker.EnumDescription();
            profilePage.ClickFirstAutoSuggestion();
            profilePage.AddMemberSubmit.ClickElement();

            Driver.ScrollToElement(profilePage.SaveProfile);
            profilePage.SaveProfile.ClickElement();

            Assert.AreEqual(membersCount + 1, profilePage.TeamMembers.Count());
            Assert.IsTrue(profilePage.TeamMembers.Any(a => a.MemberName.Text.Contains(UserType.Broker.EnumDescription())));

            profilePage.Logout();

            // Search for the team on public site locator page
            var publicSite = new HomePage();
            publicSite.FindAgentOfficeLink.ClickElement();

            var locatorPage = new LocatorPage();
            locatorPage.FindAgentsTab.ClickElement();
            Driver.WaitUntilVisible(locatorPage.LocatorNameElement, DefaultTimeout);

            locatorPage.LocatorNameInput = UserType.Team.EnumDescription();
            locatorPage.SubmitSearch.ClickElement();

            Assert.IsTrue(locatorPage.SearchResults.All(a => !a.AgentTeamName.Text.Contains(UserType.Broker.EnumDescription())), "Team assistants are showing up on team search.");
            Assert.IsTrue(locatorPage.SearchResults.Any(a => a.AgentTeamName.Text.Contains(UserType.TeamOwnerBroker.EnumDescription())), "Broker team members are not showing up on team search.");

            // Clean up
            myDesk = new MyDeskDashboard(UserType.TeamOwnerBroker);

            myDesk.Impersonate(UserType.Team, "Team owner (broker) was unable to impersonate the team.");

            myDesk.ImpersonationProfileIcon.ClickElement();
            profilePage = new TeamProfilePage();
            membersCount = profilePage.TeamMembers.Count();

            Driver.ScrollToElement(profilePage.AddMemberIcon);

            var assistant = profilePage.TeamMembers.Single(a => a.MemberName.Text.Contains(UserType.Broker.EnumDescription()));
            assistant.MemberDelete.ClickElement();

            profilePage.SaveProfile.ClickElement();
            Assert.AreEqual(membersCount - 1, profilePage.TeamMembers.Count());

            profilePage.Logout();
        }

        [TestMethod]
        public void CanAddBrokerAsTeamMember()
        {
            // Add a broker as a team assistant
            var myDesk = new MyDeskDashboard(UserType.TeamOwnerBroker);

            myDesk.Impersonate(UserType.Team, "Team owner (broker) was unable to impersonate the team.");

            myDesk.ImpersonationProfileIcon.ClickElement();
            var profilePage = new TeamProfilePage();

            // First make sure the user Broker is not currently a team member
            RemoveTeamMember(profilePage, UserType.Broker);
            var membersCount = profilePage.TeamMembers.Count();

            Driver.ScrollToElement(profilePage.AddMemberIcon);
            profilePage.AddMemberIcon.ClickElement();

            Driver.Pause(300);

            profilePage.AddMemberType.ClickElement();
            profilePage.MemberTypeBrokerOption.ClickElement();
            Driver.WaitUntilVisible(profilePage.AddBrokerElement, DefaultTimeout);

            profilePage.BrokerSearchInput = UserType.Broker.EnumDescription();
            profilePage.ClickFirstAutoSuggestion();
            profilePage.AddMemberSubmit.ClickElement();

            Driver.ScrollToElement(profilePage.SaveProfile);
            profilePage.SaveProfile.ClickElement();

            Driver.Pause();
            Driver.Navigate().Refresh();

            profilePage.WaitUntilLoadingSpinnerDisappears();
            Driver.ScrollToElement(profilePage.AddMemberIcon);

            Assert.AreEqual(membersCount + 1, profilePage.TeamMembers.Count());

            RemoveTeamMember(profilePage, UserType.Broker);
            Assert.AreEqual(membersCount, profilePage.TeamMembers.Count());

            profilePage.Logout();
        }

        [TestMethod]
        public void CanAddNonBrokerUserAsTeamAssistant()
        {
            // Add a broker as a team assistant
            var myDesk = new MyDeskDashboard(UserType.TeamOwnerBroker);

            myDesk.Impersonate(UserType.Team, "Team owner (broker) was unable to impersonate the team.");

            myDesk.ImpersonationProfileIcon.ClickElement();
            var profilePage = new TeamProfilePage();

            // First make sure the user BrokerAssistant is not currently a team member
            RemoveTeamMember(profilePage, UserType.BrokerAssistant);
            var membersCount = profilePage.TeamMembers.Count();

            Driver.ScrollToElement(profilePage.AddMemberIcon);
            profilePage.AddMemberIcon.ClickElement();

            Driver.Pause(300);

            profilePage.AddMemberType.ClickElement();
            profilePage.MemberTypeAssistantOption.ClickElement();
            Driver.WaitUntilVisible(profilePage.AddAssistantElement, DefaultTimeout);

            profilePage.AssistantSearchInput = UserType.BrokerAssistant.EnumDescription();
            profilePage.ClickFirstAutoSuggestion();
            profilePage.AddMemberSubmit.ClickElement();

            Driver.ScrollToElement(profilePage.SaveProfile);
            profilePage.SaveProfile.ClickElement();

            Driver.Pause();
            Driver.Navigate().Refresh();

            profilePage.WaitUntilLoadingSpinnerDisappears();
            Driver.ScrollToElement(profilePage.AddMemberIcon);

            Assert.AreEqual(membersCount + 1, profilePage.TeamMembers.Count());

            profilePage.Logout();

            // Assistant can impersonate the team
            myDesk = new MyDeskDashboard(UserType.BrokerAssistant);
            myDesk.Impersonate(UserType.Team, "Team Assistant was unable to impersonate the team.");

            profilePage = new TeamProfilePage();
            profilePage.Logout();

            // Clean up
            myDesk = new MyDeskDashboard(UserType.TeamOwnerBroker);
            myDesk.Impersonate(UserType.Team, "Team owner (broker) was unable to impersonate the team for cleanup.");

            myDesk.ImpersonationProfileIcon.ClickElement();
            profilePage = new TeamProfilePage();

            RemoveTeamMember(profilePage, UserType.BrokerAssistant);
            Assert.AreEqual(membersCount, profilePage.TeamMembers.Count());

            profilePage.Logout();
        }


        #region Private methods

        private void RemoveTeamMember(TeamProfilePage profilePage, UserType user)
        {
            var member = profilePage.TeamMembers.SingleOrDefault(a => a.MemberName.Text.Contains(user.EnumDescription()));

            if (member == null)
            {
                return;
            }

            Driver.ScrollToElement(member.MemberDelete);

            member.MemberDelete.ClickElement();
            Driver.Pause(300);
            profilePage.SaveProfile.ClickElement();
            Driver.Pause(300);
        }

        private void CleanupTeamMembership(MyDeskDashboard myDesk)
        {
            myDesk.ImpersonationProfileIcon.ClickElement();
            var profilePage = new TeamProfilePage();
            var membersCount = profilePage.TeamMembers.Count();

            if (membersCount == 1)
            {
                return;
            }

            var nonOwners = profilePage.TeamMembers.Where(a => !a.MemberName.Text.Equals(UserType.TeamOwnerBroker.EnumDescription())).ToList();
            foreach (var member in nonOwners)
            {
                Driver.ScrollToElement(member.MemberDelete);
                member.MemberDelete.ClickElement();
                Driver.Pause(500);
            }

            profilePage.SaveProfile.ClickElement();
            Driver.Pause(500);
        }

        #endregion
    }
}
