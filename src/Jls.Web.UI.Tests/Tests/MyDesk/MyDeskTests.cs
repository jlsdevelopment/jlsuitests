﻿using Jls.Web.UI.TestFramework;
using Jls.Web.UI.Tests.MyDesk;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UITests;
using UITests.MyDesk;

namespace Jls.Web.UI.Tests.Tests.MyDesk
{
    [TestClass]
    public class MyDeskTests : TestBase
    {
        [Ignore]
        [TestMethod]
        public void TestHyperlinks()
        {
            var dashboard = new MyDeskDashboard(UserType.Broker);

            dashboard.ContactsLink.ClickElement();
            new ContactsPage();

            dashboard.ListingManagerLink.ClickElement();
            new ListingManagerPage();

            dashboard.MediaLibraryLink.ClickElement();
            new MediaLibraryPage();

            dashboard.WebsiteDesignerLink.ClickElement();
            new WebsiteDesignerPage();

            dashboard.UserMenu.ClickElement();
            dashboard.UserMenuDashboardLink.ClickElement();
            new MyDeskDashboard(UserType.Broker);

            dashboard.UserMenu.ClickElement();
            dashboard.UserMenuProfileLink.ClickElement();
            new ProfilePage();

            dashboard.Logout();
        }
    }
}
