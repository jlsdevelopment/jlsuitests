﻿using System.Collections.Generic;
using System.Linq;
using Jls.Utility;
using Jls.Web.UI.TestFramework;
using Jls.Web.UI.Tests.MyDesk;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using UITests;
using UITests.MyDesk;

namespace Jls.Web.UI.Tests
{
    [TestClass]
    public abstract class TestBase
    {
        protected static IWebDriver Driver;
        protected static int DefaultTimeout;

        protected TestBase()
        {
            DefaultTimeout = AppConfig.Timeout();
        }

        [TestInitialize]
        public void Initialize()
        {
            JlsWebDriver.TimeoutInSeconds = DefaultTimeout = AppConfig.Timeout();
            Driver = JlsWebDriver.GetInstance();
        }

        [TestCleanup]
        public void Cleanup()
        {
            JlsWebDriver.Dispose();
        }

        public void DelegateType(UserType fromUser, UserType toUser, IEnumerable<DelegatableType> menuItems)
        {
            var list = menuItems.ToList();
            var myDesk = new MyDeskDashboard(fromUser);
            myDesk.ClickDelegationLink();

            var delegationsPage = new DelegationsPage();
            var count = delegationsPage.TargetDelegations.Count;

            delegationsPage.AddButton.ClickElement();
            Driver.WaitUntilVisible(By.Id(DelegationsPage.BrokerSearchInputId), DefaultTimeout);

            delegationsPage.BrokerSearch = toUser.EnumDescription();
            Driver.Pause();
            Assert.IsTrue(delegationsPage.BrokerSearchSuggestions.Any(),
                "The user being delegated to was not found for autocomplete suggestion.");
            delegationsPage.BrokerSearchSuggestions.First().ClickElement();

            delegationsPage.NextButton.ClickElement();
            Driver.WaitUntilVisible(By.CssSelector(DelegationsPage.CheckAllCssSelector), DefaultTimeout);

            var resourceText = new List<string>();

            foreach (var menuItem in list)
            {
                var text = menuItem.EnumDescription();
                resourceText.Add(text);
                var element = Driver.FindElement(By.CssSelector($".{text.Replace(" ", ".")}"));
                element.ClickElement();
            }

            delegationsPage.NextButton.ClickElement();
            Driver.WaitUntilVisible(By.Id(DelegationsPage.SaveButtonId), DefaultTimeout);

            foreach (var text in resourceText)
            {
                Assert.IsTrue(delegationsPage.ResourceTypesToDelegate.Any(a => a.Text == text),
                    $"Selected resource type ({text}) was not shown in the Delegations Summary.");
            }

            delegationsPage.SaveButton.ClickElement();
            Driver.WaitUntilVisible(By.ClassName(DelegationsPage.TargetDelegationsClassName), DefaultTimeout);

            Assert.IsTrue(delegationsPage.TargetDelegations.Any());
            Assert.IsTrue(delegationsPage.TargetDelegationUserNames.Any(a => a.Text == toUser.EnumDescription()));

            foreach (var text in resourceText)
            {
                Assert.IsTrue(delegationsPage.TargetDelegationsResourceTypes.Any(a => a.Text.Contains(text)));
            }

            delegationsPage.Logout();
        }
    }
}
