﻿using System.Configuration;

namespace UITests
{
    public static class AppConfig
    {
        public static int Timeout()
        {
            var value = ConfigurationManager.AppSettings["defaultTimeoutSeconds"];
            var defaultValue = 10;
            return value != null ? int.Parse(value) : defaultValue;
        }

        public static string BaseUrl()
        {
            var value = ConfigurationManager.AppSettings["baseUrl"];
            var defaultValue = "http://www.johnlscott.com";
            return value ?? defaultValue;
        }

        public static string MyDeskUrl()
        {
            var value = ConfigurationManager.AppSettings["mydeskUrl"];
            var defaultValue = "https://mydesk.johnlscott.com/";
            return value ?? defaultValue;
        }

        public static string MyDeskBrokerLogin()
        {
            var value = ConfigurationManager.AppSettings["mydeskBrokerLogin"];
            return value;
        }

        public static string MyDeskBrokerPassword()
        {
            var value = ConfigurationManager.AppSettings["mydeskBrokerPassword"];
            return value;
        }

        public static string MyDeskTeamOwnerBrokerLogin()
        {
            var value = ConfigurationManager.AppSettings["mydeskTeamOwnerBrokerLogin"];
            return value;
        }

        public static string MyDeskTeamOwnerBrokerPassword()
        {
            var value = ConfigurationManager.AppSettings["mydeskTeamOwnerBrokerPassword"];
            return value;
        }

        public static string MyDeskOfficeOwnerLogin()
        {
            var value = ConfigurationManager.AppSettings["mydeskOfcOwnerLogin"];
            return value;
        }

        public static string MyDeskOfficeOwnerPassword()
        {
            var value = ConfigurationManager.AppSettings["mydeskOfcOwnerPassword"];
            return value;
        }

        public static string MyDeskCorporateStaffLogin()
        {
            var value = ConfigurationManager.AppSettings["mydeskCorpStaffLogin"];
            return value;
        }

        public static string MyDeskCorporateStaffPassword()
        {
            var value = ConfigurationManager.AppSettings["mydeskCorpStaffPassword"];
            return value;
        }

        public static string MyDeskCorporateManagerLogin()
        {
            var value = ConfigurationManager.AppSettings["mydeskCorpMgrLogin"];
            return value;
        }

        public static string MyDeskCorporateManagerPassword()
        {
            var value = ConfigurationManager.AppSettings["mydeskCorpMgrPassword"];
            return value;
        }

        public static string MyDeskBrokerAssistantLogin()
        {
            var value = ConfigurationManager.AppSettings["mydeskBrokerAsstLogin"];
            return value;
        }

        public static string MyDeskBrokerAssistantPassword()
        {
            var value = ConfigurationManager.AppSettings["mydeskBrokerAsstPassword"];
            return value;
        }

        public static string MyDeskCampaignManagerLogin()
        {
            var value = ConfigurationManager.AppSettings["mydeskCampaignMgrLogin"];
            return value;
        }

        public static string MyDeskCampaignManagerPassword()
        {
            var value = ConfigurationManager.AppSettings["mydeskCampaignMgrPassword"];
            return value;
        }

        public static string MyDeskStandardUserLogin()
        {
            var value = ConfigurationManager.AppSettings["mydeskStandardUserLogin"];
            return value;
        }

        public static string MyDeskStandardUserPassword()
        {
            var value = ConfigurationManager.AppSettings["mydeskStandardUserPassword"];
            return value;
        }

        public static string MyDeskSupportStaffLogin()
        {
            var value = ConfigurationManager.AppSettings["mydeskSupportStaffLogin"];
            return value;
        }

        public static string MyDeskSupportStaffPassword()
        {
            var value = ConfigurationManager.AppSettings["mydeskSupportStaffPassword"];
            return value;
        }

        public static string PropertyTrackerLogin()
        {
            var value = ConfigurationManager.AppSettings["ptLogin"];
            return value;
        }

        public static string PropertyTrackerPassword()
        {
            var value = ConfigurationManager.AppSettings["ptPassword"];
            return value;
        }

        public static string ChromeDriverPath()
        {
            var value = ConfigurationManager.AppSettings["pathChromeDriver"];
            var defaultValue = ".";
            return value ?? defaultValue;
        }
    }
}
