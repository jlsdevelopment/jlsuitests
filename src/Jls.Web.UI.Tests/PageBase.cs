﻿using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;
using UITests;

namespace Jls.Web.UI.Tests
{
    public abstract class PageBase
    {
        protected string PageUrl;
        protected IWebDriver Driver;
        protected static string BaseUrl;
        protected static int DefaultTimeout;

        protected PageBase()
        {
            JlsWebDriver.TimeoutInSeconds = DefaultTimeout = AppConfig.Timeout();
            Driver = JlsWebDriver.GetInstance();
        }

        protected abstract void BrowseToPage();

        protected abstract void WaitUntilReady(int? timeout = null);
    }
}
