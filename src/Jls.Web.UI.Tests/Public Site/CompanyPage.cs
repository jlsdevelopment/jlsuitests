﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.Public_Site
{
    public class CompanyPage : PublicSitePageBase
    {
        public CompanyPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/about-johnlscott";
            if (!Driver.Url.ToLowerInvariant().Contains(PageUrl))
            {
                Driver.Navigate().GoToUrl(PageUrl);
            }
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("jls-about-hero-image"), timeout ?? DefaultTimeout, "Company page did not load in time.");
        }
    }
}
