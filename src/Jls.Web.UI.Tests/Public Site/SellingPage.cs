﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.Public_Site
{
    public class SellingPage : PublicSitePageBase
    {
        public SellingPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/selling-a-home";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                Driver.Navigate().GoToUrl(PageUrl);
            }
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("jls-seller-hero-image"), timeout ?? DefaultTimeout, "Selling page did not load in time.");
        }
    }
}
