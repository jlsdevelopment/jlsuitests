﻿using System;
using System.Collections.Generic;
using Jls.Web.UI.TestFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace UITests.Public_Site
{
    public class HomePage : PublicSitePageBase
    {
        public HomePage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        private const string SearchInputId = "search-input";
        private const string SearchButtonId = "search-btn";
        private const string SellTabId = "sell-tab";
        private const string RelocationTabId = "relo-tab";
        private const string SearchHomesId = "search-homes";
        private const string BuyingId = "buying";
        private const string SellingId = "selling";
        private const string RelocationId = "relocation";
        private const string LocatorId = "locator";
        private const string OurCompanyId = "our-company";
        private const string CareersId = "careers";
        private const string FranchiseOppId = "franchise-opportunities";
        private const string AboutId = "about-jls";
        private const string ContactUsId = "contact-us";
        private const string JoinUsId = "join-us";
        

        #region Elements
        public IWebElement SearchInputElement
        {
            get { return Driver.FindElement(By.Id(SearchInputId)); }
        }

        public string SearchInput
        {
            get { return SearchInputElement.Text; }
            set { SearchInputElement.SendKeys(value); }
        }

        public IWebElement SearchButton
        {
            get { return Driver.FindElement(By.Id(SearchButtonId)); }
        }

        public IWebElement SearchHomesLink
        {
            get { return Driver.FindElement(By.Id(SearchHomesId)); }
        }

        public IWebElement BuyingLink
        {
            get { return Driver.FindElement(By.Id(BuyingId)); }
        }

        public IWebElement SellingLink
        {
            get { return Driver.FindElement(By.Id(SellingId)); }
        }

        public IWebElement RelocationServicesLink
        {
            get { return Driver.FindElement(By.Id(RelocationId)); }
        }

        public IWebElement SellTabLink
        {
            get { return Driver.FindElement(By.Id(SellTabId)); }
        }

        public IWebElement RelocateTabLink
        {
            get { return Driver.FindElement(By.Id(RelocationTabId)); }
        }

        public IWebElement JoinUsMenu
        {
            get { return Driver.FindElement(By.Id(JoinUsId)); }
        }

        public IWebElement CareersLink
        {
            get { return Driver.FindElement(By.Id(CareersId)); }
        }

        public IWebElement FranchiseOpportunitiesLink
        {
            get { return Driver.FindElement(By.Id(FranchiseOppId)); }
        }

        public IWebElement AboutMenu
        {
            get { return Driver.FindElement(By.Id(AboutId)); }
        }

        public IWebElement OurCompanyLink
        {
            get { return Driver.FindElement(By.Id(OurCompanyId)); }
        }

        public IWebElement ContactUsLink
        {
            get { return Driver.FindElement(By.Id(ContactUsId)); }
        }

        public IWebElement FindAgentOfficeLink
        {
            get { return Driver.FindElement(By.Id(LocatorId)); }
        }

        public IEnumerable<IWebElement> ClickableStateSearches
        {
            get { return Driver.FindElements(By.ClassName("clickable-state-search")); }
        }

        #endregion

        protected override void BrowseToPage()
        {
            if (!Driver.Url.Equals(BaseUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                Driver.Navigate().GoToUrl(BaseUrl);
            }
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("home-search-links"), timeout ?? AppConfig.Timeout(), "Home page did not load in time.");
            Assert.IsTrue(Driver.Url.Contains("//www."));
        }
    }
}
