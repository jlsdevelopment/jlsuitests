﻿using System;
using Jls.Web.UI.TestFramework;
using Jls.Web.UI.Tests;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace UITests.Public_Site
{
    public abstract class PublicSitePageBase : PageBase
    {
        protected PublicSitePageBase(bool waitToLoad)
        {
            BaseUrl = AppConfig.BaseUrl();

            BrowseToPage();

            if (waitToLoad)
            {
                WaitUntilReady();
            }

            Driver.WaitUntilDomReady();
        }
    }
}
