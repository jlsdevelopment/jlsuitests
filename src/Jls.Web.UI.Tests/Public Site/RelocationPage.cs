﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.Public_Site
{
    public class RelocationPage : PublicSitePageBase
    {
        public RelocationPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/relocation";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                Driver.Navigate().GoToUrl(PageUrl);
            }
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("jls-relocation-hero-image"), timeout ?? DefaultTimeout, "Relocation page did not load in time.");
        }
    }
}
