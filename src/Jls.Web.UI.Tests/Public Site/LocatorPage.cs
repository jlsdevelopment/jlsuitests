﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.Public_Site
{
    public class LocatorPage : PublicSitePageBase
    {
        public IWebElement FindAgentsTab
        {
            get { return Driver.FindElement(By.LinkText("Find Agents")); }
        }

        public IWebElement LocatorNameElement
        {
            get { return Driver.FindElement(By.Id("locator-name-input")); }
        }

        public string LocatorNameInput
        {
            get { return LocatorNameElement.Text; }
            set { LocatorNameElement.SendKeys(value); }
        }

        public IWebElement SubmitSearch
        {
            get { return Driver.FindElement(By.Id("submit-search")); }
        }

        public ReadOnlyCollection<IWebElement> SearchResultItems
        {
            get { return Driver.FindElements(By.ClassName("jls-broker-tile")); }
        }

        public IEnumerable<AgentTeamSearchResultTile> SearchResults
        {
            get { return SearchResultItems.Select(a => new AgentTeamSearchResultTile(a)); }
        }


        public LocatorPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/locator/office";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                Driver.Navigate().GoToUrl(PageUrl);
            }
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("locator-container"), timeout ?? DefaultTimeout, "Locator page did not load in time.");
        }
    }

    public class AgentTeamSearchResultTile
    {
        private IWebElement _parent;

        public AgentTeamSearchResultTile(IWebElement element)
        {
            _parent = element;
        }

        public IWebElement Photo
        {
            get { return _parent.FindElement(By.ClassName("agent-tile-photo")); }
        }

        public IWebElement AgentTeamName
        {
            get { return _parent.FindElement(By.ClassName("agent-tile-name")); }
        }

        public IWebElement AgentTeamPhone
        {
            get { return _parent.FindElement(By.ClassName("agent-tile-phone")); }
        }

        public IWebElement AgentTeamOffice
        {
            get { return _parent.FindElement(By.ClassName("agent-tile-group")); }
        }

        public IWebElement WebsiteLink
        {
            get { return _parent.FindElement(By.ClassName("agent-tile-website-url")); }
        }

        public IWebElement EmailLink
        {
            get { return _parent.FindElement(By.ClassName("agent-tile-email")); }
        }

        public IWebElement WorkWithMeLink
        {
            get { return _parent.FindElement(By.ClassName("agent-tile-work-with")); }
        }
    }
}
