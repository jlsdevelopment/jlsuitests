﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.Public_Site.PropertyTracker
{
    public class PTProfilePage : PublicSitePageBase
    {
        public PTProfilePage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        private const string LoginId = "pt-login-btn";
        private const string LoginForm = "jls-property-tracker-login";
        private const string LoginEmailId = "jls-property-tracker-login-email";
        private const string LoginPasswordId = "jls-property-tracker-login-pw";
        private const string LoginSubmitId = "pt-login-submit";
        private const string UserMenuId = "pt-user-menu";
        private const string ParentNavId = "property-tracker-nav";
        private const string MakeFavoritesId = "make-favorites";

        public const string RecentlyViewdId = "content2";
        public const string FavoritesTabId = "fave-tab";
        public const string SearchesTabId = "searches-tab";
        public const string ProfileTabId = "profile-tab";
        public const string SettingsTabId = "settings-tab";

        public IWebElement PTLoginEmailElement
        {
            get { return Driver.FindElement(By.Id(LoginEmailId)); }
        }

        public string PTLoginEmail
        {
            get { return PTLoginEmailElement.Text; }
            set { PTLoginEmailElement.SendKeys(value); }
        }

        public IWebElement PTLoginPasswordElement
        {
            get { return Driver.FindElement(By.Id(LoginPasswordId)); }
        }

        public string PTLoginPassword
        {
            get { return PTLoginPasswordElement.Text; }
            set { PTLoginPasswordElement.SendKeys(value); }
        }

        public IWebElement PTLoginSubmit
        {
            get { return Driver.FindElement(By.Id(LoginSubmitId)); }
        }

        public IWebElement PTUserMenu
        {
            get { return Driver.FindElement(By.Id(UserMenuId)); }
        }

        public IWebElement PTLoginSignupLink
        {
            get { return Driver.FindElement(By.Id(LoginId)); }
        }

        public IWebElement FavoritesLink
        {
            get { return Driver.FindElement(By.LinkText("Favorites")); }
        }

        public IWebElement SearchesLink
        {
            get { return Driver.FindElement(By.LinkText("Searches")); }
        }

        public IWebElement ProfileLink
        {
            get { return Driver.FindElement(By.LinkText("Profile")); }
        }

        public IWebElement SettingsLink
        {
            get { return Driver.FindElement(By.LinkText("Settings")); }
        }

        public IWebElement MakeFavoritesLink
        {
            get { return Driver.FindElement(By.Id(MakeFavoritesId)); }
        }

        public IWebElement LogoutLink
        {
            get { return PTUserMenu.FindElement(By.XPath("..")).FindElement(By.PartialLinkText("Logout")); }
        }

        protected override void BrowseToPage()
        {
            if (!Driver.Url.Equals(BaseUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                Driver.Navigate().GoToUrl(BaseUrl);
            }
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("home-search-links"), timeout ?? AppConfig.Timeout(), "Home page did not load in time.");
            Login();
        }

        private void Login()
        {
            PTLoginSignupLink.ClickElement();
            Driver.WaitUntilVisible(By.Id(LoginForm), AppConfig.Timeout());

            PTLoginEmail = AppConfig.PropertyTrackerLogin();
            PTLoginPassword = AppConfig.PropertyTrackerPassword();
            PTLoginSubmit.ClickElement();

            Driver.WaitUntilVisible(By.Id(UserMenuId), AppConfig.Timeout());
        }

        public void Logoff()
        {
            PTUserMenu.ClickElement();
            LogoutLink.ClickElement();

            Driver.WaitUntilVisible(By.Id(LoginId), AppConfig.Timeout());
        }
    }
}
