﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.Public_Site
{
    public class BuyingPage : PublicSitePageBase
    {
        public BuyingPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("jls-buyer-hero-image"), timeout ?? AppConfig.Timeout(), "Buying page did not load in time.");
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/buying-a-home";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                Driver.Navigate().GoToUrl(PageUrl);
            }
        }
    }
}
