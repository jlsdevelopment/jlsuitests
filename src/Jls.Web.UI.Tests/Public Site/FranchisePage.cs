﻿using System;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.Public_Site
{
    public class FranchisePage : PublicSitePageBase
    {
        public FranchisePage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        protected override void BrowseToPage()
        {
            PageUrl = "http://johnlscottfranchise.com/";
            if (!Driver.Url.ToLowerInvariant().Contains(PageUrl))
            {
                Driver.Navigate().GoToUrl(PageUrl);
            }
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id("splendid-content"), timeout ?? DefaultTimeout, "Franchise page did not load in time.");
        }
    }
}
