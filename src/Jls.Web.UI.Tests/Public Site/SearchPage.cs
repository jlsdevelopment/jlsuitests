﻿using System;
using System.Collections.Generic;
using Jls.Web.UI.TestFramework;
using OpenQA.Selenium;

namespace UITests.Public_Site
{
    public class SearchPage : PublicSitePageBase
    {
        public SearchPage(bool waitToLoad = true) : base(waitToLoad)
        {
        }

        private const string BootboxModalSelector = ".bootbox.modal";
        public IEnumerable<IWebElement> BootboxModals
        {
            get { return Driver.FindElements(By.CssSelector(BootboxModalSelector)); }
        }

        private const string FavoriteListingClassname = "jls-list-item-fav-heart";
        public IEnumerable<IWebElement> ListingsFavoriteIcons
        {
            get { return ListingsView.FindElements(By.ClassName(FavoriteListingClassname)); }
        }

        private const string SearchMapContainerId = "map-container";
        public IWebElement SearchMap
        {
            get { return Driver.FindElement(By.Id(SearchMapContainerId)); }
        }

        public const string ListingsViewId = "listings-view";
        public IWebElement ListingsView
        {
            get { return Driver.FindElement(By.Id(ListingsViewId)); }
        }

        public const string ListingRowClass = "jls-listing-row";
        public IReadOnlyCollection<IWebElement> ListingRows
        {
            get { return Driver.FindElements(By.ClassName(ListingRowClass)); }
        }


        private const string SearchInputId = "search-input";
        public IWebElement SearchInputElement
        {
            get { return Driver.FindElement(By.Id(SearchInputId)); }
        }

        public string SearchInput
        {
            get { return SearchInputElement.Text; }
            set { SearchInputElement.SendKeys(value); }
        }

        private const string SearchButtonId = "search-btn";
        public IWebElement SearchButton
        {
            get { return Driver.FindElement(By.Id(SearchButtonId)); }
        }

        public const string AutoCompleteClassname = "autocomplete-suggestions";
        private const string AutoCompleteSuggestionClassname = "autocomplete-suggestion";
        public IEnumerable<IWebElement> AutoCompleteSuggestions
        {
            get { return Driver.FindElements(By.ClassName(AutoCompleteSuggestionClassname)); }
        }

        protected override void BrowseToPage()
        {
            PageUrl = $"{BaseUrl}/search";
            if (!Driver.Url.Equals(PageUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                Driver.Navigate().GoToUrl(PageUrl);
            }
        }

        override protected void WaitUntilReady(int? timeout = null)
        {
            Driver.WaitUntilVisible(By.Id(SearchMapContainerId), timeout ?? AppConfig.Timeout(), "Search page did not load in time.");
        }
    }
}
